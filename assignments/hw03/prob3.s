    .set noreorder
    .data
    A: .space 32
    .text
    .globl main
    .ent main
main:
#int A[8]
#int main()
#{
# int i;
	addi $s0, $0, 0 # i = 0
# A[0] = 0;
	lui $s1, %hi(A)
	ori $s1, $s1, %lo(A)
	addi $t0, $0, 0
	sw $t0, 0($s1)
# A[1] = 1;
	lui $s2, %hi(A)
	ori $s2, $s2, %lo(A)
	addi $t0, $0, 1
	addi $s2, $s2, 4
	sw $s0, 0($s2)
 
# for (i = 2; i < 8; i++) {
	addi $s0, $s0, 2 # i = 2
loop_cond:
	slti $t0, $s0, 8
	beq $t0, $0, end
	nop
# A[i] = A[i-1] + A[i-2];
	lui $t0, %hi(A) # t0 = A
	ori $t0, $t0, %lo(A)
	sll $t1, $s0, 2 # t1 = i * 4
	add $t1, $t0, $t1 # t1 = A + t1
	# store here, but first get values to store
		
# PRINT_HEX_DEC(A[i]);
# }
# EXIT;
#}
        ori $v0, $0, 10     # exit
        syscall
    .end main
