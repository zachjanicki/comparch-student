    .set noreorder
    .data
#char A[] = { 1, 25, 7, 9, -1 };
    A: .byte 1, 25, 7, 9, -1    # creation of A
    .text
    .globl main
    .ent main
main:
#int main()
#{
# int i;
# int current;
# int max;
# i = 0;
	addi $s0, $0, 0 # i = 0
# current = A[0];
	addi $s1, $0, 1 # current = A[i]
	lui $t0, %hi(A)
	ori $t0, $t0, %lo(A)
	lb $s1, 0($t0) 
# max = 0;
	addi $s2, $0, 0 # max = 0
# while (current > 0) {
loop_cond:
	slt $t1, $0, $s1 # t0 = (0 < s1)
	beq $t1, $0, end
	nop
# if (current > max)
	slt $t1, $s2, $s1 # t0 = (s2 < s1)
	beq $t1, $0, skip_if
	nop
	add $s2, $0, $s1 
# i = i + 1;
skip_if:
	addi $s0, $s0, 1 # i++
	add $t0, $t0, $s0
	lb $s1, 0($t0) # load A[i]
	j loop_cond	
# current = A[i];
end:
	add $a0, $0, $s2
        ori $v0, $0, 20
	syscall
# PRINT_HEX_DEC(max);
# EXIT;
#}
        ori $v0, $0, 10     # exit
        syscall
    .end main
