    .set noreorder
    .data
    .text
    .globl main
    .ent main

#void print(int a)
#{
# // should be implemented with syscall 20
print:  
        # add $a0, $0, $t1 # print
        ori $v0, $0, 20 # print
        syscall
        jr $ra # return to main
        nop	
#}

#int sum3(int a, int b, int c)
#{
sum3:
# return a+b+c;
	add $t5, $a0, $a1 # t0 = a + b
	add $v0, $t5, $a2 # return t0 + a2
	jr $ra # return to main
	nop
#}


#int polynomial(int a, int b, int c, int d, int e)
#{
polynomial:
	addi $sp, $sp, -4
        sw $s0, 0($sp)
# int x;
# int y;
# int z;
# x = a << b;
	sll $s0, $a0, $a1 # a << b
# y = c << d;
	sll $s1, $a2, $a3 # c << d
	add $a0, $s0, $0 # a0 = x
	add $a1, $s1, $0 # a1 = y
	lw $t0, 4($sp)
	add $a2, $t0, 0 # a2 = e
# z = sum3(x, y, e);
	jal sum3 # call sum3
	nop
	add $a0, $s0, $0 # a0 = x
# print(x);
	jal print # call print
	nop
	add $a0, $s1, $0 # a0 = y
# print(y);
	jal print # call print
	nop 
	add $a0, $v0, $0 # a0 = z
# print(z);
	jal print
	nop
	jr $ra
	
# return z;
#}
main:
#int main()
#{
# int a = 2;
	addi $s0, $0, 2 # a = 2
	addi $sp, $sp, -16 # make space for stack
	sw $ra, 12($sp) # push ra
	sw $s0, 8($sp) # push s0
	sw $s1, 4($sp) # push s1
	add $a0, $0, $s0 # create a0
	addi $a1, $0, 3 # create a1
	addi $a2, $0, 4 # create a2
	addi $a3, $0, 5 # create a3
	addi $t0, $0, 6 # t0 for last argument
	sw $t0, 0($sp)
	jal polynomial # call function
	nop
# int f = polynomial(a, 3, 4, 5, 6);
	add $s1, $v0, $0 # s1 = f
	add $a0, $s0, $0 # put s0 in a0
# print(a);
	jal print # print a
	nop
	add $a0, $s1, $0 # put s1 into a0
# print(f);
	jal print # print f
	nop
	lw $ra, 12($sp)
	lw $s0, 8($sp)
	lw $s1, 4($sp)
	addi $sp, $sp, 16
#}
        ori $v0, $0, 10     # exit
        syscall
    .end main
