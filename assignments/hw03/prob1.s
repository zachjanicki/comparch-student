    .set noreorder
    .data
    .text
    .globl main
    .ent main
main:

#int main()
#{
# int score = 84;
	addi $s0, $0, 84 # score = 84
# int grade; 
# if (score >= 90)
	slt $t0, $s0, 90 # t0 = (score < 90)
	bne $t0, $0, branch_one # if t0 go to branch
	nop
	# grade = 4;
# else if (score >= 80)
	slt $t0, $s0, 80 # t0 = (score < 80)
        bne $t0, $0, branch_two # if t0 go to branch
        nop 
	# grade = 3;
# else if (score >= 70)
	slt $t0, $s0, 70 # t0 = (score < 70)
        bne $t0, $0, branch_three # if t0 go to branch
        nop 
	beq $t0, $s0, branch_four # if t0 go to else branch
	nop
	# grade = 2;
# else
	# grade = 0;
# PRINT_HEX_DEC(grade);
# EXIT;
#}

branch_one:
	addi $s1, $0, 4 # grade = 4
	j end_branch
branch_two:
        addi $s1, $0, 3 # grade = 3
	j end_branch
branch_three:
        addi $s1, $0, 2 # grade = 2
	j end_branch
branch_four:
        addi $s1, $0, 0 # grade = 0
	j end_branch
end_branch:
	add $a0, $0, $s1
        ori $v0, $0, 20
        syscall
 
    ori $v0, $0, 10     # exit
        syscall
    .end main
