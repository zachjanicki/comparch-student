    .set noreorder
    .data
    .text
    .globl main
    .ent main
main:
#typedef struct record {
# int field0;
# int field1;
#} record;
#int main()
#{
# record *r = (record *) MALLOC(sizeof(record));
	addi $a0, $0, 8 # 8 bytes for 2 ints
	ori $v0, $0, 9
	syscall
# r->field0 = 100;
	addi $t0, $0, 100
	sw $t0, 0($v0)
# r->field1 = -1;
	addi $t1, $0, -1
	sw $t1, 4($v0)
# EXIT;
#}
        ori $v0, $0, 10     # exit
        syscall
    .end main
