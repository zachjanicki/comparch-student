    .set noreorder
    .data
    .text
    .globl main
    .ent main
main:
#typedef struct elt {
# int value;
# struct elt *next;
#} elt;
#int main()
#{
# elt *head;
# elt *newelt;
# newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $0, 8 # 4 for int, 4 for pointer
        ori $v0, $0, 9
        syscall
# newelt->value = 1;
	addi $t0, $0, 1
	sw $t0, 0($v0)
# newelt->next = 0;
	addi $t0, $0, 0 # store 0
	sw $t0, 4($v0) # put 0 into struct
# head = newelt;
	add $s0, $0, $v0 # head = newelt
# newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $0, 8
	ori $v0, $0, 9
	syscall
# newelt->value = 2;
	addi $t0, $0, 2 # value = 2
	sw $t0, 0($v0) # store value into stuct
# newelt->next = head;
	add $t0, $0, $s0 # head = s0
	sw $t0, 4($v0) # next = head
# head = newelt;
	add $s0, $0, $v0 # head = newelt
# PRINT_HEX_DEC(head->value);
	lw $t0, 0($s0) # load head -> value
	add $a0, $0, $t0 # load head -> value
	ori $v0, $0, 20 #print
	syscall
# PRINT_HEX_DEC(head->next->value);
	lw $t0, 4($s0) # load head -> next
	lw $t1, 0($t0) # load head -> next -> value
	add $a0, $0, $t1 # print
	ori $v0, $0, 20
	syscall
# EXIT;
#}        
	ori $v0, $0, 10     # exit
        syscall
    .end main
