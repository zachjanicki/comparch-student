    .set noreorder
    .data
    A: .space 32 # initialize A
    .text
    .globl main
    .ent main
print:
	# add $a0, $0, $t1 # print
        ori $v0, $0, 20 # print
        syscall
	jr $ra # return to main
	nop
#void print(int a)
#{
# // should be implemented with syscall 20
#}
main:
#int main()
#{
# int A[8];
# int i;
	lui $t0, %hi(A) # load A
	ori $t0, $t0, %lo(A) # load rest of A 
# A[0] = 0;
	addi $t1, $0, 0 # initialize a 0
	sw $t1, 0($t0) # save into A[0]
# A[1] = 1;
	addi $t1, $0, 1 # initialize a 1
	sw $t1, 4($t0) # save into A[1]
# for (i = 2; i < 8; i++) {
	addi $s0, $0, 2 # i = 2
for_loop:
	slti $t2, $s0, 8 # i < 8
	beq $t2, $0, end # i is not less than 8
	nop
# A[i] = A[i-1] + A[i-2];
	sll $t3, $s0, 2 # i * 4
	add $t4, $t0, $t3 # t4 = &A[i]
	lw $t1, -4($t4) # t1 = A[i - 1]
	lw $t2, -8($t4) # t2 = A[i - 2]
	add $t3, $t1, $t2 # t1 + t2
	sw $t3, 0($t4) # store t3 into A[i]
	add $a0, $t3, $0 # send A[i] to print function 
# print(A[i]);
	jal print #call print function
	nop
	addi $s0, $s0, 1 #i++
	j for_loop #back to top of loop
	nop
# }
#}
end:
	ori $v0, $0, 10     # exit
        syscall
    .end main
